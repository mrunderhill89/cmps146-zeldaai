# The set of languages for which implicit dependencies are needed:
SET(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
SET(CMAKE_DEPENDS_CHECK_CXX
  "/home/chase/git/zelda/cmps146-zeldaai/solarus-1.1.0/src/lowlevel/Main.cc" "/home/chase/git/zelda/cmps146-zeldaai/solarus-1.1.0/CMakeFiles/solarus.dir/src/lowlevel/Main.cc.o"
  )
SET(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
SET(CMAKE_TARGET_DEFINITIONS
  "SOLARUS_COLOR_DEPTH=32"
  "SOLARUS_DEFAULT_QUEST=\".\""
  "SOLARUS_DEFAULT_QUEST_HEIGHT=240"
  "SOLARUS_DEFAULT_QUEST_WIDTH=320"
  "SOLARUS_SCREEN_DOUBLEBUF"
  "SOLARUS_SCREEN_FORCE_MODE=-1"
  "SOLARUS_WRITE_DIR=\".solarus\""
  )

# Targets to which this target links.
SET(CMAKE_TARGET_LINKED_INFO_FILES
  "/home/chase/git/zelda/cmps146-zeldaai/solarus-1.1.0/CMakeFiles/solarus_static.dir/DependInfo.cmake"
  )

# The include file search paths:
SET(CMAKE_C_TARGET_INCLUDE_PATH
  "include"
  "include/snes_spc"
  "/usr/include/SDL"
  "/usr/include/AL"
  "/usr/include/vorbis"
  "/usr/include/ogg"
  "/usr/include/libmodplug"
  "/usr/include/lua5.1"
  )
SET(CMAKE_CXX_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
SET(CMAKE_Fortran_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
SET(CMAKE_ASM_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
