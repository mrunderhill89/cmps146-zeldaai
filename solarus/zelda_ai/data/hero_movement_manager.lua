io.stderr:write("LOADING: hero_movement_manager.lua\n") -- debug

hero_current_movement_direction = {}
hero_last_pos = nil
hero_last_room = nil

-- Returns the nearest pos on the grid from the given position
function get_nearest_grid_pos(current_pos, grid_size)
	--io.stderr:write("CALLED: get_nearest_grid_pos\n") -- debug
    local game = sol.main.game
	local map = game:get_map()
    
	local current_x, current_y = pos_to_coord(current_pos)
    local remainder_x = current_x % grid_size
    local remainder_y = current_y % grid_size
    
    local best_distance, test_distance = nil, nil
    local best_pos, test_pos = nil, nil
    local test_x, test_y
    
    -- io.stderr:write("   current_pos: " .. current_pos .. "\n") -- debug
    -- io.stderr:write("   remainder_x: " .. remainder_x .. "\n") -- debug
    -- io.stderr:write("   remainder_y: " .. remainder_y .. "\n") -- debug
    
    -- If the current_pos is not on the grid
    if remainder_x ~= 0 or remainder_y ~= 0 then
        --io.stderr:write("   test each of the four possible corners\n") -- debug
        -- Test each of the four potential points to snap to
        --io.stderr:write("   test up, left\n") -- debug
        -- Up, Left
        test_x = current_x - remainder_x
        test_y = current_y - remainder_y
        test_pos = coord_to_pos(test_x, test_y)
        test_distance = distance(current_pos, test_pos)
        if (best_pos == nil or best_distance > test_distance == true) and map:get_ground(test_x, test_y, 0) == "traversable" then
            best_distance = test_distance
            best_pos = test_pos
        end
        
        --io.stderr:write("   test up, right\n") -- debug
        -- Up, Right
        test_x = current_x + (grid_size - remainder_x)
        test_y = current_y - remainder_y
        test_pos = coord_to_pos(test_x, test_y)
        test_distance = distance(current_pos, test_pos)
        if (best_pos == nil or best_distance > test_distance == true) and map:get_ground(test_x, test_y, 0) == "traversable" then
            best_distance = test_distance
            best_pos = test_pos
        end
        
        --io.stderr:write("   test down, left\n") -- debug
        -- Down, Left
        test_x = current_x - remainder_x
        test_y = current_y + (grid_size - remainder_y)
        test_pos = coord_to_pos(test_x, test_y)
        test_distance = distance(current_pos, test_pos)
        if (best_pos == nil or best_distance > test_distance == true) and map:get_ground(test_x, test_y, 0) == "traversable" then
            best_distance = test_distance
            best_pos = test_pos
        end
        
        --io.stderr:write("   test down, right\n") -- debug
        -- Down, Right
        test_x = current_x + (grid_size - remainder_x)
        test_y = current_y + (grid_size - remainder_y)
        test_pos = coord_to_pos(test_x, test_y)
        test_distance = distance(current_pos, test_pos)
        if (best_pos == nil or best_distance > test_distance == true) and map:get_ground(test_x, test_y, 0) == "traversable" then
            best_distance = test_distance
            best_pos = test_pos
        end
    else
        best_pos = current_pos
    end
    
    return best_pos
end

-- Snaps hero to grid and returns position
function snap_hero_to_grid(grid_size)
	--io.stderr:write("CALLED: snap_hero-to_grid\n") -- debug
    local game = sol.main.game
    local hero = game:get_hero()
    
	local hero_x, hero_y = hero:get_position()
    local hero_pos = coord_to_pos(hero_x, hero_y)
    
    local hero_grid_pos = get_nearest_grid_pos(hero_pos, grid_size)
    local hero_grid_x, hero_grid_y = pos_to_coord(hero_grid_pos)
    
    hero:set_position(hero_grid_x, hero_grid_y)
    
    return hero_grid_pos   
end

function update_keys(new_movement_direction)
    local game = sol.main.game
    if hero_current_movement_direction["up"] ~= new_movement_direction["up"] then
        if new_movement_direction["up"] == nil then
            game:force_command_released("up")
        else
            game:force_command_pressed("up")
        end
    end
    
    if hero_current_movement_direction["down"] ~= new_movement_direction["down"] then
        if new_movement_direction["down"] == nil then
            game:force_command_released("down")
        else
            game:force_command_pressed("down")
        end
    end
    
    if hero_current_movement_direction["right"] ~= new_movement_direction["right"] then
        if new_movement_direction["right"] == nil then
            game:force_command_released("right")
        else
            game:force_command_pressed("right")
        end
    end
    
    if hero_current_movement_direction["left"] ~= new_movement_direction["left"] then
        if new_movement_direction["left"] == nil then
            game:force_command_released("left")
        else
            game:force_command_pressed("left")
        end
    end
end

-- Resets A* and movement manager variables and clears keys
function clear_hero_movement()
    -- Reset A* variables
    current_grid_path_child_list = {}
    path_end_pos = nil
    path_next_pos = nil

    -- Clear keys and reset movement manager variables
    local clear_keys = {}
    update_keys(clear_keys)
    hero_last_pos = nil
    hero_last_map = nil
    hero_current_movement_direction = {} 
end

function update_hero_movement()
    --io.stderr:write("CALLED: update_hero_movement\n") -- debug
    if path_end_pos == nil then
        -- Path has not been set, nothing to do (maybe calculate path should go here?)
        --io.stderr:write("   Path has not been calculated yet. Do nothing.\n") -- debug
    else
        -- Local variables
        local game = sol.main.game
        local hero = game:get_hero()
        local hero_current_map = game:get_map():get_id()
        
        local hero_xpos, hero_ypos = hero:get_position()
        local hero_pos = tostring(hero_xpos) .. " " .. tostring(hero_ypos)
        --io.stderr:write("   hero_pos = " .. tostring(hero_pos) .. "\n") -- debug      
        
        -- Set hero_last_map if it hasn't been set yet
        if hero_last_map == nil then
            hero_last_map = hero_current_map
        end
        
        -- Check if reached goal or changed room (ie. hit teletransporter)
        if hero_pos == path_end_pos or hero_last_map ~= hero_current_map then
            io.stderr:write("   reached goal, reset variables\n") -- debug
            clear_hero_movement() 
        end
        
        if hero_pos ~= hero_last_pos then
            --print_hero_movement_data() -- debug
            io.stderr:write("   hero_pos has changed\n") -- debug

            -- -- Snap hero to next_pos if close enough
            -- local distance_to_next_pos = distance(hero_pos, path_next_pos) -- function from a_star.lua
            -- if distance_to_next_pos <= 2 then -- Comparison here represents pixels
                -- local x, y = pos_to_coord(path_next_pos)
                -- hero:set_position(x,y)
                -- io.stderr:write("   hero snapped to pos: " .. path_next_pos .. "\n") -- debug
            -- end
            
            -- Get new movement direction
            local new_movement_direction = get_movement_to_next_pos(hero_pos) -- function from a_star.lua
            io.stderr:write("   new_movement_direction = ") -- debug
            for i,v in pairs(new_movement_direction) do
                io.stderr:write(tostring(v) .. ", ")
            end
            io.stderr:write("\n")
            
            -- Update keys
            update_keys(new_movement_direction)
            
            -- Update current movement direction
            hero_current_movement_direction = new_movement_direction
            io.stderr:write("   hero_current_movement_direction is now: ") -- debug
            for i,v in pairs(hero_current_movement_direction) do
                io.stderr:write(tostring(v) .. ", ")
            end
            io.stderr:write("\n")
            
            -- Update hero_last_pos
            hero_last_pos = hero_pos
            io.stderr:write("   hero_last_pos is now: " .. hero_last_pos .. "\n") -- debug
        end
    end
end

function print_hero_movement_data()
    io.stderr:write("   -------------------------------------\n")
    print_current_grid_path_variables() -- function from a_star.lua
    
    io.stderr:write("   Hero movement data:\n")
    local hero_xpos, hero_ypos = sol.main.game:get_hero():get_position()
    local hero_pos = tostring(hero_xpos) .. " " .. tostring(hero_ypos)
    io.stderr:write("      hero_pos: " .. hero_pos .. "\n")
    io.stderr:write("      hero_last_pos: " .. tostring(hero_last_pos) .. "\n")
    io.stderr:write("      hero_current_movement_direction: ")
    for i,v in pairs(hero_current_movement_direction) do
        io.stderr:write(tostring(v) .. ", ")
    end
    io.stderr:write("\n")
end

function print_map_traversable_list(grid_size)
    io.stderr:write("-------------------------------------\n")
    io.stderr:write("Printing map traversable list\n")
    
    local map = sol.main.game:get_map()
    local x_size, y_size = map:get_size()
    
    io.stderr:write("x_size: " .. x_size .. " y_size: " .. y_size .. "\n")
    
    for x_pos = 0, x_size - 1, grid_size do
        for y_pos = 0, y_size - 1, grid_size do
            io.stderr:write("   Pos: " .. x_pos .. " " .. y_pos .. " -- " .. tostring(map:get_ground(x_pos, y_pos, 0)) .. "\n")
        end
    end 

    io.stderr:write("finished\n")
end
