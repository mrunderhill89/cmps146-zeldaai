local game = ...
local metrics_file

local dead = false
local metrics = {}
metrics.manualFrames = 0
metrics.score = 0
metrics.map_count = -1
metrics.backtrack_count = 0
metrics.maps_visited = {}
metrics.maps_visited_unique = {}

-- Include the various game features.
sol.main.load_file("dungeons")(game)
sol.main.load_file("equipment")(game)
sol.main.load_file("menus/pause")(game)
sol.main.load_file("menus/dialog_box")(game)
sol.main.load_file("menus/game_over")(game)
sol.main.load_file("hud/hud")(game)

require('DungeonPlanner')

-- Load main class for the Zelda AI
sol.main.do_file("link_ai") -- load ai for link

-- Useful functions for this specific quest.

function game:on_started()

  -- Set up the dialog box and the HUD.
  self:initialize_dialog_box()
  self:initialize_hud()
  
  if (metrics_file ~= nil) then
	metrics_file:close()
  end
end

function game:on_finished()

  -- Clean what was created by on_started().
  self:quit_hud()
  self:quit_dialog_box()
  write_metrics("Game Exited")
  metrics_file:close()
  metrics_file = nil
end

path_set_flag = false
-- This event is called when a new map has just become active.
function game:on_map_changed(map)
  metrics.map_count = metrics.map_count + 1
  add_visted_map(map:get_id())
  
  -- Notify the hud.
  self:hud_on_map_changed(map)
  
  -- Dungeon Planner code
  if self.dungeonPlanner == nil then
      self.dungeonPlanner = DungeonPlanner.create()
  end

  self.dungeonPlanner:enterRoom(self, map)
  local hero = self:get_hero()
  hero.target_x, hero.target_y = self.dungeonPlanner:getCoordinates(self)
  hero.target_y = hero.target_y + 8
end

function game:on_paused()
  self:hud_on_paused()
  self:start_pause_menu()
  print("coords to move to:", self.dungeonPlanner:getCoordinates(self))
end

function game:on_unpaused()
  self:stop_pause_menu()
  self:hud_on_unpaused()
end

function game:get_player_name()
  return self:get_value("player_name")
end

function game:set_player_name(player_name)
  self:set_value("player_name", player_name)
end

function game:on_update()
	if (self.link_ai ~= nil) then
		self.link_ai.args.metrics = metrics
		self.link_ai:update(self)
    else
        self.link_ai = Link_AI:new()
	end
	
	if (metrics_file == nil and not dead) then
		local file_name = os.date("metrics_%d-%m-%y_%H%M.txt")
		metrics_file = io.open(file_name, "w")
	end
	
	if (not dead) then
		metrics.score = metrics.score + 1
	end
	
	--print("                               " .. self:get_life())
	
	if self:get_life() <= 0 then
		--print("                               This should print when link dies")
		if not dead then
			--print("                               This should also print when link dies")
			dead = true
		  	write_metrics("Hero Death")
		  	metrics_file:close()
			metrics_file = nil
		end
	else
		dead = false
	end
end

-- Returns whether the current map is in the inside world.
function game:is_in_inside_world()
  return self:get_map():get_world() == "inside_world"
end

-- Returns whether the current map is in the outside world.
function game:is_in_outside_world()
  return self:get_map():get_world() == "outside_world"
end

-- Returns whether the current map is in a dungeon.
function game:is_in_dungeon()
  return self:get_dungeon() ~= nil
end

-- Writes some metrics about the current state to metrics.txt
function write_metrics(game_status)
	print("                               WRITING " .. game_status)
  	
  	metrics_file:write("\n", game_status)
  	metrics_file:write("\nFrames spent alive: ", metrics.score)
	metrics_file:write("\nFrames under manual control: ", metrics.manualFrames)
	if (metrics.score > 0) then
		metrics_file:write("\n% of time under manual control: ", 100 * metrics.manualFrames / metrics.score, "%")
	end
  	local hearts = game:get_life() / 4
  	metrics_file:write("\nHearts Left: ", hearts)
  	metrics_file:write("\nNumber of doors entered: ", metrics.map_count)
  	metrics_file:write("\nNumber of rooms visited: ", #metrics.maps_visited_unique)
  	metrics_file:write("\nNumber of backtracks: ", metrics.backtrack_count)
  	metrics_file:write("\nUnique room ID's: ")
  	for i = 1, #metrics.maps_visited_unique, 1 do
  		metrics_file:write(metrics.maps_visited_unique[i], " ")
  	end
  	metrics_file:write("\n\nOrder that rooms were visited: ")
  	for i = 1, #metrics.maps_visited, 1 do
  		metrics_file:write(metrics.maps_visited[i], " ")
  	end
  	metrics_file:write("\n\n")
end

-- Adds a map id to the list of room id's in the metrics table
function add_visted_map(map_id)
	  	
  	-- check if the hero beat the dungeon
  	if map_id == "0" then
	  	for i = 1, #metrics.maps_visited_unique, 1 do
	  		if metrics.maps_visited_unique[i] == "room12" then
	  			write_metrics("Hero Win")
	  			reset_metrics()
	  			break
	  		end
	  	end
  	end
  	
	-- add room
	table.insert(metrics.maps_visited, map_id)

	-- add unique room
  	for i = 1, #metrics.maps_visited_unique, 1 do
  		if metrics.maps_visited_unique[i] == map_id then
  			metrics.backtrack_count = metrics.backtrack_count + 1
  			return
  		end
  	end

  	table.insert(metrics.maps_visited_unique, map_id)
  	
end

function reset_metrics()
	metrics.score = 0
	metrics.manualFrames = 0
	metrics.map_count = -1
	metrics.backtrack_count = 0
	metrics.maps_visited = {}
	metrics.maps_visited_unique = {}

end

-- Run the game.
sol.main.game = game
game:start()

