local game = ...

-- Define the existing dungeons and their floors for the minimap menu.
game.dungeons = {
  [1] = {
    floor_width = 1040,
    floor_height = 696,
    lowest_floor = -1,
    highest_floor = 1,
    maps = { "room1", "room2", "room3", "room4" },
    boss = {
      floor = -1,
      x = 648,
      y = 144,
      savegame_variable = "b63",
    },
  }
}

-- Make a redundant mapping: map id -> dungeon index
game.dungeon_indexes = {}
for index, dungeon in pairs(game.dungeons) do
  for _, map_id in ipairs(dungeon.maps) do
    game.dungeon_indexes[map_id] = index
  end
end

-- Returns the index of the current dungeon if any, or nil.
function game:get_dungeon_index()
  local map_id = self:get_map():get_id()
  return self.dungeon_indexes[map_id]
end

-- Returns the current dungeon if any, or nil.
function game:get_dungeon()

  local index = self:get_dungeon_index()
  return self.dungeons[index]
end

function game:is_dungeon_finished(dungeon_index)
  return self:get_value("dungeon_" .. dungeon_index .. "_finished")
end

function game:set_dungeon_finished(dungeon_index, finished)
  if finished == nil then
    finished = true
  end
  self:set_value("dungeon_" .. dungeon_index .. "_finished", finished)
end

function game:has_dungeon_map(dungeon_index)

  dungeon_index = dungeon_index or self:get_dungeon_index()
  return self:get_value("dungeon_" .. dungeon_index .. "_map")
end

function game:has_dungeon_compass(dungeon_index)

  dungeon_index = dungeon_index or self:get_dungeon_index()
  return self:get_value("dungeon_" .. dungeon_index .. "_compass")
end

function game:has_dungeon_big_key(dungeon_index)

  dungeon_index = dungeon_index or self:get_dungeon_index()
  return self:get_value("dungeon_" .. dungeon_index .. "_big_key")
end

function game:has_dungeon_boss_key(dungeon_index)

  dungeon_index = dungeon_index or self:get_dungeon_index()
  return self:get_value("dungeon_" .. dungeon_index .. "_boss_key")
end

