io.stderr:write("LOADING: a_star.lua\n") -- debug

current_grid_path_child_list = {} -- global variable that stores a list of the children of each pos in the path generated by A*
path_end_pos = nil -- global variable that stores the last pos of the path (should always be target pos)
path_next_pos = nil -- global variable that stores the next target pos of the path

-- Return movement toward next pos and update global variables if needed
function get_movement_to_next_pos(hero_pos)
    --io.stderr:write("\nCALLED: get_movement_to_next_pos\n") -- debug
    --io.stderr:write("   hero_pos = " .. tostring(hero_pos) .. "\n") -- debug
	if hero_pos == path_end_pos then
		return {} -- Already at target
	end
	
    -- Set next pos if it is not set
    if path_next_pos == nil and hero_pos ~= path_end_pos then
        path_next_pos = current_grid_path_child_list[hero_pos]
    end
    
	if hero_pos == path_next_pos then
		path_next_pos = current_grid_path_child_list[path_next_pos] -- update next pos
	end
    
	return get_movement_direction(hero_pos, path_next_pos) -- get movement direction
end

-- Functions for converting between string representation of coordinate
-- and position representation (ie. (69, 120) <--> "69 120")
function pos_to_coord(pos)
    --io.stderr:write("\nCALLED: pos_to_coord\n") -- debug
    --io.stderr:write("   pos = " .. tostring(pos) .. "\n") -- debug
    local x_pos = nil
    local y_pos = nil
    
    local flag = 1
    for w in string.gmatch(pos, "[^%s]+") do
        if flag == 1 then
            x_pos = tonumber(w)
        else
            y_pos = tonumber(w)
        end
        
        flag = flag + 1
    end

    --io.stderr:write("   x_pos = " .. tostring(x_pos) .. "\n") -- debug
    --io.stderr:write("   y_pos = " .. tostring(y_pos) .. "\n") -- debug
    
    return x_pos, y_pos
end

function coord_to_pos(x_pos, y_pos)
    return tostring(x_pos) .. " " .. tostring(y_pos)
end

-- Checks that tile lies within bounds
function valid_pos(pos, x_size, y_size)
    local x, y = pos_to_coord(pos)
	-- io.stderr:write(type(x) .. " " .. type(y) .. "\n") -- debug
	-- io.stderr:write(type(x_size) .. " " .. type(y_size) .. "\n") -- debug
    if x < 0 or x > (x_size - 1) or y < 0 or y > (y_size - 1) then
        return false
    end
    
    return true
end

-- -- Get all eight neighbor positions and return as table -- UNFINISHED
-- function get_connections(pos, grid_size, x_size, y_size)
	-- local x_pos, y_pos = pos_to_coord(pos)
	-- local test_connection = nil
	-- local return_connections = {}
	
	-- -- Check up
	-- test_connection = coord_to_pos(x_pos, y_pos - grid_size)
	-- if valid_pos(test_connection, x_size, y_size) then
		-- return connections[test_connection] = test_connection
	-- end
	
	-- -- Check up, left
	-- test_connection = coord_to_pos(
-- end

-- Checks that the position is traversable and far enough from walls
function is_traversable(pos, direction, connections)
	local x_pos, y_pos = pos_to_coord(pos)
    
    -- Hardcoded values for 16 x 16 hero bounding box (origin at 8,13; assume zero indexed...)
	local neg_x = 8
    local pos_x = 7
    local neg_y = 13
    local pos_y = 2
	
	-- Test if this pos is traversable
	local map = sol.main.game:get_map() -- Grab map for checking if positions are traversable
	if map:get_ground(x_pos, y_pos, 0) == "traversable" then
		-- Check each neighbor in direction of movement
		if direction["up"] == "up" then
			-- Check up
			if map:get_ground(x_pos, (y_pos - neg_y), 0) ~= "traversable" then
				return false
			end
			
			-- Check up, left
			if map:get_ground(x_pos - neg_x, (y_pos - neg_y), 0) ~= "traversable" then
				return false
			end
			
			-- Check up, right
			if map:get_ground(x_pos + pos_x, (y_pos - neg_y), 0) ~= "traversable" then
				return false
			end
        end
        
		if direction["left"] == "left" then
			-- Check left
			if map:get_ground(x_pos - neg_x, y_pos, 0) ~= "traversable" then
				return false
			end
			
			-- Check up, left
			if map:get_ground(x_pos - neg_x, (y_pos - neg_y), 0) ~= "traversable" then
				return false
			end
			
			-- Check down, left
			if map:get_ground(x_pos - neg_x, (y_pos + pos_y), 0) ~= "traversable" then
				return false
			end
        end
        
		if direction["right"] == "right" then
			-- Check right
			if map:get_ground(x_pos + pos_x, y_pos, 0) ~= "traversable" then
				return false
			end
			
			-- Check up, right
			if map:get_ground(x_pos + pos_x, (y_pos - neg_y), 0) ~= "traversable" then
				return false
			end
			
			-- Check down, right
			if map:get_ground(x_pos + pos_x, (y_pos + pos_y), 0) ~= "traversable" then
				return false
			end
        end
        
		if direction["down"] == "down" then
			-- Check down
			if map:get_ground(x_pos, (y_pos + pos_y), 0) ~= "traversable" then
				return false
			end
			
			-- Check down, left
			if map:get_ground(x_pos - neg_x, (y_pos + pos_y), 0) ~= "traversable" then
				return false
			end
			
			-- Check down, right
			if map:get_ground(x_pos + pos_x, (y_pos + pos_y), 0) ~= "traversable" then
				return false
			end
		end
    else
        -- Ground at pos is not traversable
        return false
	end
		
	-- Pos is traversable
	return true
end

-- -- Checks that the position is traversable and far enough from walls
-- function is_traversable(pos, direction, connections)
	-- local x_pos, y_pos = pos_to_coord(pos)
	-- local hero = sol.main.game:get_hero()
    -- local origin_x_offset, origin_y_offset = hero:get_origin() -- origin_x_offset not used
	-- local x_offset, y_offset = hero:get_size()
    -- x_offset = x_offset/2 -- Slight overestimate
	-- y_offset = y_offset/2
	-- -- x_offset = (x_offset/2) - 1 -- Subtract the single pixel that represents the hero's origin
	-- -- y_offset = (y_offset/2) - 1
    -- -- x_offset = 0 -- debug
	-- -- y_offset = 0 --debug
    -- origin_y_offset = origin_y_offset - y_offset
	
	-- -- Test if this pos is traversable
	-- local map = sol.main.game:get_map() -- Grab map for checking if positions are traversable
	-- if map:get_ground(x_pos, y_pos, 0) == "traversable" then
		-- -- Check each neighbor in direction of movement
		-- if direction["up"] == "up" then
			-- -- Check up
			-- if map:get_ground(x_pos, (y_pos - y_offset - origin_y_offset), 0) ~= "traversable" then
				-- return false
			-- end
			
			-- -- Check up, left
			-- if map:get_ground(x_pos - x_offset, (y_pos - y_offset - origin_y_offset), 0) ~= "traversable" then
				-- return false
			-- end
			
			-- -- Check up, right
			-- if map:get_ground(x_pos + x_offset, (y_pos - y_offset - origin_y_offset), 0) ~= "traversable" then
				-- return false
			-- end
        -- end
        
		-- if direction["left"] == "left" then
			-- -- Check left
			-- if map:get_ground(x_pos - x_offset, y_pos, 0) ~= "traversable" then
				-- return false
			-- end
			
			-- -- Check up, left
			-- if map:get_ground(x_pos - x_offset, (y_pos - y_offset - origin_y_offset), 0) ~= "traversable" then
				-- return false
			-- end
			
			-- -- Check down, left
			-- if map:get_ground(x_pos - x_offset, (y_pos + y_offset - origin_y_offset), 0) ~= "traversable" then
				-- return false
			-- end
        -- end
        
		-- if direction["right"] == "right" then
			-- -- Check right
			-- if map:get_ground(x_pos + x_offset, y_pos, 0) ~= "traversable" then
				-- return false
			-- end
			
			-- -- Check up, right
			-- if map:get_ground(x_pos + x_offset, (y_pos - y_offset - origin_y_offset), 0) ~= "traversable" then
				-- return false
			-- end
			
			-- -- Check down, right
			-- if map:get_ground(x_pos + x_offset, (y_pos + y_offset - origin_y_offset), 0) ~= "traversable" then
				-- return false
			-- end
        -- end
        
		-- if direction["down"] == "down" then
			-- -- Check down
			-- if map:get_ground(x_pos, (y_pos + y_offset - origin_y_offset), 0) ~= "traversable" then
				-- return false
			-- end
			
			-- -- Check down, left
			-- if map:get_ground(x_pos - x_offset, (y_pos + y_offset - origin_y_offset), 0) ~= "traversable" then
				-- return false
			-- end
			
			-- -- Check down, right
			-- if map:get_ground(x_pos + x_offset, (y_pos + y_offset - origin_y_offset), 0) ~= "traversable" then
				-- return false
			-- end
		-- end
    -- else
        -- -- Ground at pos is not traversable
        -- return false
	-- end
		
	-- -- Pos is traversable
	-- return true
-- end

-- Gets adjacent positions
-- **Sensitive to grid size (could cause errors)
function get_connections(pos, grid_size, x_size, y_size)
    local x, y = pos_to_coord(pos)
    local test_connection = nil
    local return_connections = {}
    
	-- Check up
    test_connection = coord_to_pos(x, y - grid_size)
    if valid_pos(test_connection, x_size, y_size) then
        return_connections[test_connection] = test_connection
    end
	-- Check up, right
    test_connection = coord_to_pos(x + grid_size, y - grid_size)
    if valid_pos(test_connection, x_size, y_size) then
        return_connections[test_connection] = test_connection
    end
	-- Check up, left
    test_connection = coord_to_pos(x - grid_size, y - grid_size)
    if valid_pos(test_connection, x_size, y_size) then
        return_connections[test_connection] = test_connection
    end
    -- Check down
    test_connection = coord_to_pos(x, y + grid_size)
    if valid_pos(test_connection, x_size, y_size) then
        return_connections[test_connection] = test_connection
    end
	-- Check down, right
    test_connection = coord_to_pos(x + grid_size, y + grid_size)
    if valid_pos(test_connection, x_size, y_size) then
        return_connections[test_connection] = test_connection
    end
	-- Check down, left
    test_connection = coord_to_pos(x - grid_size, y + grid_size)
    if valid_pos(test_connection, x_size, y_size) then
        return_connections[test_connection] = test_connection
    end
    -- Check right
    test_connection = coord_to_pos(x + grid_size, y)
    if valid_pos(test_connection, x_size, y_size) then
        return_connections[test_connection] = test_connection
    end
    -- Check below
    test_connection = coord_to_pos(x - grid_size, y)
    if valid_pos(test_connection, x_size, y_size) then
        return_connections[test_connection] = test_connection
    end
    
    return return_connections
end

-- Get euclidean distance between two positions
function distance(pos1, pos2)
    if pos1 == nil or pos2 == nil then
        return nil
    end

    local x1, y1 = pos_to_coord(pos1)
    local x2, y2 = pos_to_coord(pos2)
    
    return math.sqrt(((x2 - x1)*(x2 - x1)) + ((y2 - y1)*(y2 - y1)))
end

-- Pull min from table
function min_element(open_list, estimated_total_cost_list)
    local best_element = nil
    local best_cost = nil
    for i,v in pairs(open_list) do
        if best_element == nil or estimated_total_cost_list[v] < best_cost then
            best_element = v
            best_cost = estimated_total_cost_list[v]
        end
    end
    
    return best_element
end

-- Returns a string corresponding to movement between two positions
function get_movement_direction(from_pos, to_pos)
    --io.stderr:write("\nCALLED: get_movement_direction\n") -- debug
    --io.stderr:write("   from_pos = " .. tostring(from_pos) .. "\n") -- debug
    --io.stderr:write("   to_pos = " .. tostring(to_pos) .. "\n") -- debug
    
    local direction_table = {} -- table to store the direction
    
    -- Return none if invalid input or reached goal
    if from_pos == nil or to_pos == nil then
        return direction_table
    end
    
    local x1, y1 = pos_to_coord(from_pos)
    local x2, y2 = pos_to_coord(to_pos)
    
    -- Check cardinal directions
    if x2 > x1 then
        direction_table["right"] = "right" -- move right
    end
    
    if x2 < x1 then
        direction_table["left"] = "left" -- move left
    end
    
    if y2 > y1 then
        direction_table["down"] = "down" -- move down
    end
    
    if y2 < y1 then
        direction_table["up"] = "up" -- move up
    end
    
    -- if x2 > x1 then
        -- return "right" -- move right
    -- elseif x2 < x1 then
        -- return "left" -- move left
    -- elseif y2 > y1 then
        -- return "down" -- move down
    -- else
        -- return "up" -- move up
    -- end
    
    return direction_table
end

-- Gets path from hero to target in room using position and offset for nodes
function a_star_grid_make_path(hero_pos, target_pos, grid_size)
    io.stderr:write("CALLED: a_star_grid_make_path\n") -- debug
    local map = sol.main.game:get_map() -- Grab map for checking if positions are traversable
    local map_xsize = nil
    local map_ysize = nil
    map_xsize, map_ysize = map:get_size()
    local non_traversable_list = {} -- Ref list of all discovered positions that are not traversable
    grid_size = grid_size or 8 -- Sets defaul grid size if no param given
    
    io.stderr:write("   map_id: " .. tostring(map:get_id()) .. "\n") -- debug
    
    -- Lists to store data about the nodes (positions)
    local open_list = {} -- both key and value are the same
    local open_list_size = 0
    local closed_list = {} -- both key and value are the same
    local cost_so_far_list = {} -- cost indexed by position value
    local estimated_total_cost_list = {} -- cost indexed by position value
    local connection_list = {} -- cost indexed by position value
    
    -- Put start node on list
    open_list[hero_pos] = hero_pos
    open_list_size = 1
    cost_so_far_list[hero_pos] = 0
    estimated_total_cost_list[hero_pos] = distance(hero_pos, target_pos)
    
    io.stderr:write("   entering main A* while loop\n") -- debug
    -- Iterate through processing each node
    local current_pos = nil
    local current_xpos = nil
    local current_ypos = nil
    local connections = {}
    while open_list_size > 0 do
        -- Find smallest element on open list
        current_pos = min_element(open_list, estimated_total_cost_list)
        current_xpos, current_ypos = pos_to_coord(current_pos)
        io.stderr:write("-----------------------------------------\n") -- debug
        io.stderr:write("      checking pos: " .. current_pos .. "\n") -- debug
        io.stdout:write("      checking pos: " .. current_pos .. "\n") -- debug
        
        io.stderr:write("      current pos == target pos? ") -- debug
        if current_pos == target_pos then
            io.stderr:write("true\n") -- debug
        else
            io.stderr:write("false\n") -- debug
        end
        
        -- If it is the goal node, then terminate
        if current_pos == target_pos then
            io.stderr:write("      FOUND TARGET POS\n") -- debug
            break
        end
        
        io.stderr:write("      get outgoing connections for current_pos\n") -- debug
        -- Otherwise get its outgoing connections
        connections = get_connections(current_pos, grid_size, map_xsize, map_ysize)
        
        io.stderr:write("      entering 'for' loop for connections\n") -- debug
        -- Loop through each connection in turn
        local next_pos_cost = nil
        local next_pos_heuristic = nil
        local skip_flag = false
        for i,next_pos in pairs(connections) do
            io.stderr:write("----------\n") -- debug
            io.stderr:write("         checking connection: " .. next_pos .. "\n") -- debug
            io.stderr:write("         check that connection is traversable\n") -- debug
            -- Check that each connection is traversable
            if non_traversable_list[next_pos] == nil then -- haven't seen yet
                local traversable_test = is_traversable(next_pos, get_movement_direction(current_pos, next_pos), connections)
                if traversable_test then
                    non_traversable_list[next_pos] = "good"
                else
                    non_traversable_list[next_pos] = "bad"
                end
            end
            
            if non_traversable_list[next_pos] == "good" then -- pos is traversable, continue on
                io.stderr:write("         connection is traversable\n") -- debug
                -- Get the cost estimate for the node
                next_pos_cost = (cost_so_far_list[current_pos] + 1) -- cost is always uniform
                
                -- If the pos is closed, we may have to skip, or remove it from
                -- the closed list
                if closed_list[next_pos] ~= nil then
                    io.stderr:write("         connection is on closed list\n") -- debug
                    -- Check cost versus old recorded cost
                    if cost_so_far_list[next_pos] > next_pos_cost then
                        io.stderr:write("         found better path, take off closed list\n") -- debug
                        closed_list[next_pos] = nil -- romove from closed list
                        next_pos_heuristic = (next_pos_cost - cost_so_far_list[next_pos])
                    else
                        skip_flag = true
                    end
                -- Skip if the pos is open and we've not found a better route
                elseif open_list[next_pos] ~= nil then
                    io.stderr:write("         connection is on open list\n") -- debug
                    if cost_so_far_list[next_pos] > next_pos_cost then
                        next_pos_heuristic = (next_pos_cost - cost_so_far_list[next_pos])
                    else
                        skip_flag = true
                    end
                -- Otherwise we know we've got an unvisited pos, calculate heuristic
                else
                    io.stderr:write("         connection is new\n") -- debug
                    next_pos_heuristic = distance(next_pos, target_pos)
                end
                
                -- Update lists and add to open list if needed
                if skip_flag == false then
                    io.stderr:write("         update lists\n") -- debug
                    cost_so_far_list[next_pos] = next_pos_cost
                    estimated_total_cost_list[next_pos] = next_pos_cost + next_pos_heuristic
                    connection_list[next_pos] = current_pos
                    
                    io.stderr:write("         cost_so_far_list[" .. next_pos .. "] = " .. next_pos_cost .. "\n") -- debug
                    io.stderr:write("         estimated_total_cost_list[" .. next_pos .. "] = " .. next_pos_cost + next_pos_heuristic .. "\n") -- debug
                    io.stderr:write("         connection_list[" .. next_pos .. "] = " .. current_pos .. "\n") -- debug
                    
                    if open_list[next_pos] == nil then
                        io.stderr:write("         add node to open list\n") -- debug
                        open_list[next_pos] = next_pos
                        open_list_size = open_list_size + 1
                    end   
                else
                    skip_flag = false
                end             
            else
                io.stderr:write("         connection is non-traversable\n") -- debug
            end
        end
        io.stderr:write("      finished looking at connections, remove pos from open list and add to closed list\n") -- debug
        -- We've finished looking at the connections for the current pos, so 
        -- add it to the closed list and remove if from the open list
        open_list[current_pos] = nil
        open_list_size = open_list_size - 1
        closed_list[current_pos] = current_pos
    end
    
    io.stderr:write("   finished A* main while loop\n") -- debug
    -- We're here if we've either found the goal, or if we've no more pos to search
    if current_pos ~= target_pos then
        io.stderr:write("   A* failed to find a path to the target\n") -- debug
        current_grid_path_child_list = {}
		path_end_pos = nil
		path_next_pos = nil
    else
        io.stderr:write("   update global variables with path data\n") -- debug
        -- Update global variables with path data
		path_end_pos = current_pos
		
        if connection_list[current_pos] ~= nil then -- used to avoid error if A* called when already at target
            local break_flag = false
            local child = nil
            io.stderr:write("   entering loop over connection_list to build current_grid_path_child_list\n") -- debug
            while true do
                io.stderr:write("      looking at pos: " .. current_pos .. "\n") -- debug
                if connection_list[current_pos] == nil then
                    break_flag = true
                end
                
                if current_pos == target_pos then -- set nil for child of target pos
                    current_grid_path_child_list[current_pos] = nil
                    current_grid_path_child_list[connection_list[current_pos]] = current_pos
                    current_pos = connection_list[current_pos]
                elseif current_pos ~= hero_pos then -- set child for each pos (avoid nil error with hero pos)
                    child = current_pos
                    current_pos = connection_list[current_pos]
                    current_grid_path_child_list[current_pos] = child
                end
                
                if break_flag then
                    break
                end
            end
        end
    end
    io.stderr:write("FINISHED: a_star_grid_make_path\n") -- debug
end

function print_current_grid_path(hero_pos)
	io.stderr:write("Path from hero position (" .. tostring(hero_pos) .. ") to " ..
                    "target position (" .. tostring(path_end_pos) .. "): \n")
	
	local tmp_pos = hero_pos
	
	while current_grid_path_child_list[tmp_pos] ~= nil do
        io.stderr:write("from " .. tmp_pos .. " to " .. current_grid_path_child_list[tmp_pos] .. " ")
        for i,v in pairs(get_movement_direction(tmp_pos, current_grid_path_child_list[tmp_pos])) do
            io.stderr:write(tostring(v) .. ", ")
        end
        io.stderr:write("\n")
		tmp_pos = current_grid_path_child_list[tmp_pos]
	end
end

function print_current_grid_path_variables()
    io.stderr:write("   Current grid path variables:\n")
    if path_end_pos ~= nil then
        io.stderr:write("      path_end_pos:" .. tostring(path_end_pos) .. "\n")
        io.stderr:write("      path_next_pos:" .. tostring(path_next_pos) .. "\n")
    else
        io.stderr:write("      Grid path has not been calculated.\n")
    end
end



















