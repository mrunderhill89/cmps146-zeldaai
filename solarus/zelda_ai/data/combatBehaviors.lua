--[[
    Conditions
]]--

function has_sword(args)
    --return true if the hero has a sword equipped, otherwise false
    return true
end

function sword_charging(args)
    --return true if the hero is charging a sword attack, otherwise false.
    return true
end

function sword_charged(args)
    --return true if the sword is fully charged, otherwise false.
    return true
end

function has_bombs(args)
    --return true if the hero has bombs equipped, otherwise false
    return true
end