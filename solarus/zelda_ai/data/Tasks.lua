io.stderr:write("LOADING: Tasks.lua\n") -- debug
count = 0

-- Simply attack with the sword
function Link_AI:AttackWithSword(args)
	local game = args.game
	assert(game ~= nil, "AttackWithSword received nil as its game value")
	print("Attacking with sword")
    if (self.swinging == nil) then
        self.swinging = false
    end
    if (self.swinging == false) then
        game:force_command_pressed("attack")
        self.swinging = true
        sol.timer.start(game, 500, function()
            game:force_command_released("attack")
            self.swinging = false
        end)
    end
	return true	
end

-- If there's only one enemy, then we don't really need this
function Link_AI:CheckNearestEnemy(args)
	print("Finding nearest enemy")
end

-- Maybe we can skip this
function Link_AI:CheckEnemyReachable(args)
	print("Checking for path to enemy")
end

-- Condition to switch to Combat State
function Link_AI:EnemyPresent(args)
	local map = args.map
	assert(map ~= nil, "EnemyPresent received nil as its map value")
	return map:has_entities("enemy")
end 

-- Condition to switch to Explore State
function Link_AI:NotEnemyPresent(args)
	local map = args.map
	assert(map ~= nil, "NotEnemyPresent received nil as its map value")
	return not map:has_entities("enemy")
end

-- See if there is still an enemy to be attacked
function Link_AI:StillAlive(args)
	print("Checking if enemy still alive")
	local map = args.map
	assert(map ~= nil, "StillAlive received nil as its map value")
	local enemy = map:has_entities("enemy")
	if(enemy) then return true
	else return false
	end
end

-- Find a path to the enemy
function Link_AI:PathToEnemy(args)
    if (not Link_AI:EnemyPresent(args)) then
        return false
    end
    print("Pathfinding to enemy")
    for k,v in pairs(args) do
        print("Pathfinding Argument:"..k)
    end
    local map = args.map
    assert(map ~= nil, "PathToEnemy received nil as its map value")
    local game = args.game
    assert(game ~= nil, "PathToEnemy received nil as its game value")
    local hero = args.hero
    assert(hero ~= nil, "PathToEnemy received nil as its hero value")
    
    -- Snap hero to the grid (if needed)
    local grid_hero_pos = snap_hero_to_grid(8)
    io.stderr:write("grid_hero_pos: " .. grid_hero_pos .. "\n") -- debug

    -- Get the enemy entity
    local enemy
    for e in map:get_entities("enemy") do
        enemy = e
        break
    end
    assert(enemy ~= nil, "Failed to get enemy position")
    
    -- Make the enemy position the target
    local enemy_xpos, enemy_ypos = enemy:get_position()
    local hero_target_pos = coord_to_pos(enemy_xpos, enemy_ypos)
    local grid_target_pos = get_nearest_grid_pos(hero_target_pos, 8)
    io.stderr:write("grid_target_pos: " .. grid_target_pos .. "\n") -- debug
    
    -- Clear old path data (just in case)
    clear_hero_movement()
    
    -- Calculate path to enemy using A*
    a_star_grid_make_path(grid_hero_pos, grid_target_pos)
    if (path_end_pos == nil) then
        print("Failed to create path to enemy")
        return false
    end
    
    -- Save enemy position
    args.target_pos = grid_target_pos
    
    return true
end

-- Once we have a path, follow it.
function Link_AI:MoveToEnemy(args)
    if (args.target_pos == nil) then
        print("MoveToEnemy: No target position")
        return false
    end
	local map = args.map
	assert(map ~= nil, "MoveToEnemy received nil as its map value")
	local game = args.game
	assert(game ~= nil, "MoveToEnemy received nil as its game value")
	local hero = args.hero

    -- Check if hero is close enough to the enemy
    local hero_xpos, hero_ypos = hero:get_position()
    local hero_pos = coord_to_pos(hero_xpos, hero_ypos)
    if Link_AI:EnemyInRange(args) then
        return true
    end
    
    -- Otherwise, move continue to move toward the target
    local moved, error = pcall(update_hero_movement())
    if (moved) then
        return "wait"
    else
        print("Hero Movement Error:"..error)
        return false
    end
end

function Link_AI:EnemyInRange(args)
	local map = args.map
	assert(map ~= nil, "EnemyPresent received nil as its map value")
	local enemy
	for e in map:get_entities("enemy") do
		enemy = e
		break
	end	
	local hero = args.hero
	local dist = enemy:get_distance(hero)
	if(dist < 20) then
		print("Enemy in attacking range")
		return true
	else 
		print("enemy not in range")
		return false
	end	
end

--[[
function Avoid(args)
	print("Using path away from enemy")
	local hero_xpos, hero_ypos = hero:get_position()
    local hero_grid_xpos = math.floor(hero_xpos / 8) * 8
    local hero_grid_ypos = math.floor(hero_ypos / 8) * 8
    local hero_pos = pos_to_string(hero_grid_xpos, hero_grid_ypos)
	a_star_grid_make_path(hero_pos, target_pos)
	update_hero_movement()
end
]]--