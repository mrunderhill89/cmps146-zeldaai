FSM = {}
FSM.__index = FSM

function FSM:new()
	local fsm = {}
	setmetatable(fsm,FSM)
	fsm.states = {}
	fsm.initial = nil
	fsm.current = nil
	fsm.transitions = {}
	return fsm
end

State = {}
State.__index = State

function State:new()
	local s = {}
	setmetatable(s,State)
	s.onEntry = nil
	s.onUpdate = nil
	s.onExit = nil
	s.transitions = {}
	return s
end

Transition = {}
Transition.__index = Transition

function Transition:new(_to,_condition,_action)
	local t = {}
	setmetatable(t,Transition)
	assert(_to ~= nil, "Transition has a null target state.")
	assert(type(_condition) == "function", "Transition needs a function for its condition. Got "..type(_condition).." instead.")
	assert((_action == nil) or (type(_action) == "function"), "Transition needs either nil a function for its action. Got "..type(_action).." instead.")
	t.to = _to
	t.condition = _condition
	t.action = _action
	return t
end

function Transition:test(args)
	return self:condition(args)
end

function Transition:run(args)
	return self:action(args)
end

function FSM:addState(s)
	assert(type(s) == "table", "Only tables (preferably states) can be added to an FSM. Got "..type(s).." instead.")
	table.insert(self.states, s)
	if (self.initial == nil) then
		self.initial = s
	end
end

function State:addTransition(trans)
	assert(type(trans) == "table", "Only tables (preferably transitions) can be added to a state's transitions. Got "..type(trans).." instead.")
	table.insert(self.transitions, trans)
end

function FSM:addTransition(trans)
	assert(type(trans) == "table", "Only tables (preferably transitions) can be added to an FSM's global transitions. Got "..type(trans).." instead.")
	table.insert(self.transitions, trans)
end

function FSM:update(args)
    assert(args ~= nil,"FSM Update being passed a nil args list")
	--First handle if the FSM is just starting up.
	if (self.current == nil) then --Set current state to initial if nil
		if (self.initial == nil) then --If there is no initial state, we have an empty FSM.
			io.stderr.write("FSM contains no states")
			return
		else
			self.current = self.initial
			if (self.current.onEntry ~= nil) then
				self.current:onEntry(args)
			end
		end
	end
	-- Handle global transitions
	-- These are useful if we want the FSM to drop whatever it's doing and go to a state immediately.
	local trans = nil -- assume that no transition is triggered at first
	for k,t in ipairs(self.transitions) do
		local result = t:test(args)
		if (result == true and self.current ~= t.to) then
			trans = t
			break
		end
	end
	-- Handle the current state's transitions
	if (trans == nil) then
		for k,t in ipairs(self.current.transitions) do 
			local result = t:test(args)
			if (result == true) then
				trans = t
				break
			end
		end
	end
	
	if (trans ~= nil) then	-- If we caught a transition, handle it.
			if (self.current.onExit ~= nil) then
				self.current:onExit(args)
			end
			self.current = trans.to
			if (trans.action ~= nil) then
				trans:action(args)
			end
			if (self.current.onEntry ~= nil) then
				self.current:onEntry(args)
			end			
	else 	-- Otherwise, just call the update function and exit
		if (self.current.onUpdate ~= nil) then
			self.current:onUpdate(args)
		end			
	end
end

--[[
print("Create the state machine")
statemachine = FSM.new()
print("Create states and functions for entry")

stateA = State.new()
stateA.onEntry = function() print("Entering State A") end
stateA.onUpdate = function() print("Updating State A") end
stateA.onExit = function() print("Exiting State A") end

stateB = State.new()
stateB.onEntry = function() print("Entering State B") end
stateB.onUpdate = function() print("Updating State B") end
stateB.onExit = function() print("Exiting State B") end

print("Add the states to the state machine")
statemachine:addState(stateA)
statemachine:addState(stateB)

print("Add transitions between states")
AtoB = Transition:new(stateB,alwaysTrue,printAB)
BtoA = Transition:new(stateA,alwaysTrue,printBA)
stateA:addTransition(AtoB)
stateB:addTransition(BtoA)

print("First Update")
actions = statemachine:update()

print("Second Update")
actions = statemachine:update()

print("Third Update")
actions = statemachine:update()
]]--