Graph = {
    nodes_xpos_data = {},
    nodes_ypos_data = {},
    nodes_cost_data = {},
    connections = {},
    
    num_nodes = 0,
    num_connections = 0
}

function Graph:new(o)
    o = o or {}
    setmetatable(o,self)
    self.__index = self
    return o
end

function Graph:add_node(id, x_pos, y_pos)
    --io.stderr:write("\nCalled add_node\n") -- debug
    
    self.nodes_xpos_data[id] = x_pos
    self.nodes_ypos_data[id] = y_pos
    self.nodes_cost_data[id] = nil
    self.num_nodes = self.num_nodes + 1
    
    -- io.stderr:write("node x_pos: " .. self.nodes_xpos_data[id] .. "\n") -- debug
    -- io.stderr:write("node y_pos: " .. self.nodes_ypos_data[id] .. "\n") -- debug
    -- io.stderr:write("node cost: " .. tostring(self.nodes_cost_data[id]) .. "\n") -- debug
    -- io.stderr:write("----------------------------------------------\n") -- debug
end

function Graph:add_connection(from_node_id, to_node_id)
    if self.connections[from_node_id] == nil then
        self.connections[from_node_id] = {}
    end
    
    (self.connections[from_node_id])[to_node_id] = to_node_id
    self.num_connections = self.num_connections + 1
end

function Graph:get_connections(node)
    return self.connections[node]
end

local game = sol.main.game
function Graph:build_generic_graph()

end

function Graph:remove_obstacles()

end

function Graph:reset()
    self.nodes_xpos_data = {},
    self.nodes_ypos_data = {},
    self.nodes_cost_data = {},
    self.connections = {},
    
    self.num_nodes = 0,
    self.num_connections = 0
end
