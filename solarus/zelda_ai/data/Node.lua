io.stderr:write("LOADING: Node.lua\n") -- debug

Node = {}
Node.__index = Node

function Node.create(nodeID, parentNode)
    local self = setmetatable({}, Node)
    self.ID = nodeID
    self.parentNode = parentNode
    self.adjacentNodes = {}
    self.undiscoveredIDs = {}
    self.numAdjacentNodes = 0
    self.adjacentIndex = 0

    self.doors = {}
    self.openedDoors = {}

    self.visited = false
    return self
end

function Node:addAdjacentNode(node)
    self:removeUndiscoveredID(node.ID)
    self.numAdjacentNodes = self.numAdjacentNodes + 1
    self.adjacentNodes[self.numAdjacentNodes] = node
end

function Node:removeUndiscoveredID(id)
    local length = table.getn(self.undiscoveredIDs)
    if length <= 0 then return end

    for i = 1, (length + 1) do
        if self.undiscoveredIDs[i] == id then
            table.remove(self.undiscoveredIDs, i)
        end
    end
end

function Node:numDoorsOpened(map)
    local numOpen = table.getn(self.openedDoors)
    for door in map:get_entities("door") do
        if door:is_open() == true then self:addOpenDoor(door) end
    end
    return table.getn(self.openedDoors) - numOpen
end

function Node:addDoors(map)
    self.doors = {}
    for door in map:get_entities("door") do
        self.doors[table.getn(self.doors) + 1] = door
    end
    return table.getn(self.doors)
end

function Node:addOpenDoor(door)
    for i,d in pairs(self.openedDoors) do
        if door == d then return end
    end
    self.openedDoors[table.getn(self.openedDoors) + 1] = door
end
