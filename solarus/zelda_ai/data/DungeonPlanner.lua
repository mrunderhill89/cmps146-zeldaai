io.stderr:write("LOADING: DungeonPlanner.lua\n") -- debug

require('DungeonSearch')

io.stderr:write("LOADING: DungeonPlanner.lua\n") -- debug

DungeonPlanner = {}
DungeonPlanner.__index = DungeonPlanner

function DungeonPlanner.create()
    local self = setmetatable({}, DungeonPlanner)
    self.dungeonSearch = DungeonSearch.create()
    self.tmpDeadEnds = {}

    self.destNode = nil
    self.x = 0
    self.y = 0

    self.unexploredNodes = 1
    self.unopenedDoors = 0

    return self
end

function DungeonPlanner:getCoordinates()
    return self.x, self.y
end

function DungeonPlanner:enterRoom(game, map)
    local visited = self.dungeonSearch:getNode(map:get_id())
    self.unexploredNodes = self.unexploredNodes + self.dungeonSearch:visitNode(game, map)

    local node = self.dungeonSearch:getCurrentNode()
    if node == self.destNode then self.destNode = nil end

    if visited == nil then
        self.unexploredNodes = self.unexploredNodes - 1
        self.unopenedDoors = self.unopenedDoors + node:addDoors(map)

        if table.getn(node.doors) > 0 then
            self.tmpDeadEnds[table.getn(self.tmpDeadEnds) + 1] = node
        end
    end

    self:chooseXY(game)
end

function DungeonPlanner:updateRoom(game, map)
    local node = self.dungeonSearch:getNode(map:get_id())
    self.unopenedDoors = self.unopenedDoors - node:numDoorsOpened(map)
    print("unopened doors:", self.unopenedDoors)

    if table.getn(node.doors) == table.getn(node.openedDoors) then self:removeDeadEnd(node.ID) end
    self:chooseXY(game)
end

-- Choose an x, y pair that isn't blocked by a door
function DungeonPlanner:chooseXY(game)
    local node = self.dungeonSearch.currentNode
    if node == self.destNode then self.destNode = nil end

    --node.adjacentIndex = 0

    -- Go to destNode if we have one
    if self.destNode ~= nil then
        self.x, self.y = self.dungeonSearch:nextMoveTowardNode(game, self.destNode.ID)
    else
        -- If all nodes not behind doors have been explored, then go towards the first door
        print(self.unexploredNodes, self.unopenedDoors)
        if self.unexploredNodes <= self.unopenedDoors then
            local numDeadEnds = table.getn(self.tmpDeadEnds)
            if numDeadEnds > 0 then
                self.destNode = self.tmpDeadEnds[1]
                -- Push the first node to the end of the list
                if numDeadEnds > 1 then
                    table.remove(self.tmpDeadEnds, 1)
                    self.tmpDeadEnds[numDeadEnds] = self.destNode
                end
                self.x, self.y = self.dungeonSearch:nextMoveTowardNode(game, self.destNode.ID)
                return
            end
        end
        -- Otherwise, go to the next unexplored node
        self:nextXY(game)
    end
end

-- Get the coordinates of the next valid teletransporter
function DungeonPlanner:nextXY(game)
    self.x, self.y = self.dungeonSearch:nextMovePosition(game)
    for i = 1, 4 do
        local again = false
        for i,door in ipairs(self.dungeonSearch.currentNode.doors) do
            local dx, dy = door:get_position()
            if dx == self.x and dy == self.y then
                self.x, self.y = self.dungeonSearch:nextMovePosition(game)
                again = true
                break
            end
        end
        if again == false then break end
    end
end

function DungeonPlanner:removeDeadEnd(nodeID)
    for i = 1, table.getn(self.tmpDeadEnds) do
        if self.tmpDeadEnds[i].ID == nodeID then
            table.remove(self.tmpDeadEnds, i)
            return
        end
    end
end
