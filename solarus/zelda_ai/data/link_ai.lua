io.stderr:write("LOADING: link_ai.lua\n") -- debug

sol.main.do_file("a_star") -- load A* script
sol.main.do_file("hero_movement_manager") -- load movement manager script
sol.main.do_file("fsm") -- load finite state machine
sol.main.do_file("behaviorTree") -- load behavior tree script

Link_AI = {}
Link_AI.__index = Link_AI

sol.main.do_file("Tasks") -- load tasks script

function Link_AI:new(o)
    --io.stderr:write("CALLED: Link_AI:new\n") -- debug
    local ai = o or {}
    setmetatable(ai,Link_AI)
    ai.fsm = Link_AI:initialize_fsm()
    ai.game = nil
    ai.hero = nil
    ai.map = nil
    ai.args = {}
    return ai
end

function Link_AI:update(game)
    --io.stderr:write("CALLED: Link_AI:update\n") -- debug
    if (self.fsm == nil) then
        self.fsm = Link_AI:initialize_fsm()
    end
    
    self.game = game
    assert(self.game ~= nil, "Link AI: Game variable is nil.")
    
    self.hero = game:get_hero()
    self.map = game:get_map()
    
    self.args.game = self.game
    self.args.map = self.map
    self.args.hero = self.hero
    assert(self.args.game ~= nil, "Link AI: Game variable is nil.")
    assert(self.args.hero ~= nil, "Link AI: Hero variable is nil.")
    assert(self.args.map ~= nil, "Link AI: Map variable is nil.")
	assert(self.args.metrics ~= nil, "Link AI: Metric variable is nil.")
    self.fsm:update(self.args)
end

-- Finite State Machine
function Link_AI:initialize_fsm()
    --io.stderr:write("CALLED: Link_AI:initialize_fsm\n") -- debug
    local fsm = FSM.new()
    
    --A blank state we can enter manually in case we need to stop the AI (delete key?)
    state_stop = State.new()
    state_stop.onEntry = function(self, args) 
                            print("ASSUMING DIRECT CONTROL")
                            local game = args.game
                            if (game ~= nil) then
                                game:force_command_released("up")
                                game:force_command_released("left")
                                game:force_command_released("down")
                                game:force_command_released("right")
                            end
                        end
	state_stop.onUpdate = function(self, args)
                            for k,v in pairs(args) do
                                print("Stop Argument:"..k)
                            end
                            if (args.metrics ~= nil) then
                                args.metrics.manualFrames = args.metrics.manualFrames + 1
                                print("# of frames:"..args.metrics.manualFrames)
                            end
                        end
    state_stop.onExit = function(self, args) print("RELEASING CONTROL") end
    
    state_explore = State:new()
    state_explore.onEntry = self.state_explore_entry
    state_explore.onExit = self.state_explore_exit
    state_explore.onUpdate = self.state_explore_update
    
    state_combat = State:new()
    state_combat.onEntry = self.state_combat_entry
    state_combat.onExit = self.state_combat_exit
    state_combat.onUpdate = self.state_combat_update
    
    assert(Link_AI.EnemyPresent ~= nil, "EnemyPresent set to nil in Link_AI")
    trans_combat = Transition:new(state_combat, Link_AI.EnemyPresent, nil)
    assert(Link_AI.NotEnemyPresent ~= nil, "NotEnemyPresent set to nil in Link_AI")
    trans_explore = Transition:new(state_explore, Link_AI.NotEnemyPresent, nil)
    
    state_explore:addTransition(trans_combat)
    state_combat:addTransition(trans_explore)
    
    -- Create behavior tree for combat state (please generate only once)
    local checkAlive = BT:make(Link_AI.StillAlive)
    local pathToEnemy = BT:make(Link_AI.PathToEnemy)
    local moveToEnemy = BT:make(Link_AI.MoveToEnemy)
    local attack = BT:make(Link_AI.AttackWithSword)

    state_combat.tree = BT:make(BT.slicesequence)
    state_combat.tree:addChild(checkAlive)
    state_combat.tree:addChild(pathToEnemy)
    state_combat.tree:addChild(moveToEnemy)
    state_combat.tree:addChild(attack)

    fsm:addState(state_explore)
    fsm:addState(state_combat)
    fsm:addState(state_stop)
    
    forceExplore = Transition:new(state_explore, function() return sol.input.is_key_pressed("kp 1") end, nil)
    forceCombat = Transition:new(state_combat, function() return sol.input.is_key_pressed("kp 2") end, nil)
    forceStop = Transition:new(state_stop, function() return sol.input.is_key_pressed("kp 0") end, nil)
    
    fsm:addTransition(forceExplore)
    fsm:addTransition(forceCombat)
    fsm:addTransition(forceStop)
    
    return fsm
end

function Link_AI:state_explore_entry(args)
    print("Entering Explore State.")
    for k,v in pairs(args) do
        print("Explore Argument:"..k)
    end
end

function Link_AI:state_explore_exit()
    clear_hero_movement()
end

function Link_AI:state_explore_update(args)
    --io.stderr:write("CALLED: state_explore_update\n") -- debug
    assert(args ~= nil, "Args table got set to nil")
    local hero = args.hero
    local map = args.map
    assert(hero ~= nil, "Explore: Hero variable got set to nil.")
    if path_end_pos == nil then
        local grid_hero_pos = snap_hero_to_grid(8)
        io.stderr:write("grid_hero_pos: " .. grid_hero_pos .. "\n") -- debug
        
        local hero_target_pos = coord_to_pos(hero.target_x, hero.target_y)
        local grid_target_pos = get_nearest_grid_pos(hero_target_pos, 8)
        
        -- local grid_target_pos = "48 120" -- debug
        --print_map_traversable_list(8) -- debug
        
        -- io.stderr:write("hero.target_x: " .. hero.target_x .. "\n") -- debug
        -- io.stderr:write("hero.target_y: " .. hero.target_y .. "\n") -- debug
        -- io.stderr:write("hero_target_pos: " .. hero_target_pos .. "\n") -- debug
        io.stderr:write("grid_target_pos: " .. grid_target_pos .. "\n") -- debug
        
        if grid_target_pos ~= nil then
            a_star_grid_make_path(grid_hero_pos, grid_target_pos, 8)		
            print_current_grid_path(grid_hero_pos)
        end
        
    end
    
    update_hero_movement()
end

function Link_AI:state_combat_entry(args)
    print("Entering Combat State.")
    for k,v in pairs(args) do
        print("Combat Argument:"..k)
    end
end

function Link_AI:state_combat_exit()
    print("Exiting Combat State.")
    clear_hero_movement()
end

function Link_AI:state_combat_update(args)
    self.tree:run(args)
end

function test_eUpdate()

end




