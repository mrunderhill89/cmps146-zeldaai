require('Node')

io.stderr:write("LOADING: DungeonSearch.lua\n") -- debug

DungeonSearch = {}
DungeonSearch.__index = DungeonSearch

function DungeonSearch.create()
    local self = setmetatable({}, DungeonSearch)
    self.graph = {}
    self.numNodes = 0
    self.currentNode = nil
    return self
end

function DungeonSearch:getCurrentNode()
    return self.currentNode
end

-- Update the graph
-- Should be called each time the hero enters a room
-- (Put in the game:on_map_changed() method in the play_game.lua file)
-- Returns the number of unexplored rooms connected to this one
function DungeonSearch:visitNode(game, map)
    local mapID = map:get_id()
    local node = self:getNode(mapID)
    if node == nil then
        self.numNodes = self.numNodes + 1
        node = Node.create(mapID, self.currentNode)
        self.graph[self.numNodes] = node
        if self.currentNode ~= nil then
            self.currentNode:addAdjacentNode(node)
        end
    end

    local currentNode = node

    if currentNode.visited == true then
        self.currentNode = currentNode
        return 0
    end

    currentNode.visited = true

    local i = 1
    for trans in map:get_entities("tele") do
        local parentID = '-1'
        if currentNode.parentNode ~= nil then parentID = currentNode.parentNode.ID end

        if trans:get_map_id() ~= parentID then
            local node = self:getNode(trans:get_map_id())
            if node ~= nil then
                currentNode:addAdjacentNode(node)
                node:addAdjacentNode(currentNode)
            else
                currentNode.undiscoveredIDs[i] = trans:get_map_id()
                i = i + 1
            end
        end
    end

    self.currentNode = currentNode
    return (i - 1) -- The number of unexplored dungeon nodes
end

-- Call to get the coordinaate pair, (x, y), to move to
-- in order to get to the next unexplored room or the starting
-- room if all rooms have been explored
-- Call repeatedly to get locations of all unexplored exits in the room
function DungeonSearch:nextMovePosition(game)
    if self.currentNode == nil then return nil end
    local node = self.currentNode

    print("search next move position")
    print("index:",self.currentNode.adjacentIndex)
    node.adjacentIndex = (node.adjacentIndex % table.getn(node.undiscoveredIDs)) + 1
    print("index:",node.adjacentIndex)
    print("maxnum:",table.getn(node.undiscoveredIDs))

    local trans = self:getTeletransporter(game, node.undiscoveredIDs[node.adjacentIndex])
    if trans == nil and node.parentNode ~= nil then
        trans = self:getTeletransporter(game, node.parentNode.ID)
    end

    if trans == nil then return nil
    else return trans:get_center_position() end
end

-- This is mostly an internal method, you shouldn't need to call it
function DungeonSearch:getNode(nodeID)
    for i = 1, self.numNodes do
        if nodeID == self.graph[i].ID then
            return self.graph[i]
        end
    end
    return nil
end

-- Same with this one
function DungeonSearch:getTeletransporter(game, id)
    for trans in game:get_map():get_entities("tele") do
        if trans:get_map_id() == id then
            return trans
        end
    end
    return nil
end

-- Djikstra Stuff
-- Warning: Only searches the known graph, not the whole map

-- Get the coordinate pair, (x, y), to move to in order to
-- move towards to given node ID
function DungeonSearch:nextMoveTowardNode(game, nodeID)
    local dist = {}     -- These are all key-value pairs, not arrays
    local parent = {}   -- The keys are the nodeIDs of the associated nodes
    local visited = {}

    dist[self.currentNode.ID] = 0
    local openSet = { self.currentNode }

    while table.getn(openSet) > 0 do
        local curr = openSet[1]
        table.remove(openSet, 1)
        visited[curr.ID] = true

        -- Check for the target node and loop backwards to decide which way to go
        if curr.ID == nodeID then
            local par = parent[curr.ID]
            if par == nil then return nil end

            local id = curr.ID
            while parent[par.ID] ~= nil do
                id = par.ID
                par = parent[par.ID]
            end

            local trans = self:getTeletransporter(game, id)
            if trans == nil then return nil
            else return trans:get_center_position() end
        end
        -- end

        for i,adj in pairs(curr.adjacentNodes) do
            if visited[adj.ID] == nil then
                dist[adj.ID] = dist[curr.ID] + 1
                parent[adj.ID] = curr
                openSet[table.getn(openSet) + 1] = adj
            end
        end

        if curr.parentNode ~= nil then
            if visited[curr.parentNode.ID] == nil then
                dist[curr.parentNode.ID] = dist[curr.ID] + 1
                parent[curr.parentNode.ID] = curr
                openSet[table.getn(openSet) + 1] = curr.parentNode
            end
        end
    end

    return nil
end
